clear 
clc

%params   
%zeta1 = log(0.25)/sqrt((pi^2)+ log(0.25)^2);

%based on an Mpv of 25%
zeta2 = -log(0.25)/sqrt((pi^2)+ log(0.25)^2);
%based on 5% settling time of 1.5s 
wn = -log(0.05*sqrt(1-zeta2^2))/(zeta2*1.5);
%performance specification bounds (upper)
performance_spec = tf([0 1],[(1/wn)^2 2*zeta2/wn 1]);


critical_sampling_freq = 2*pi*1/0.1; %probably unnecessary

%A = [6.60 3.90 2.21];
%Tau = [0.85 0.55 0.35];
%defining P

c = 1; 
A = 6.60; 
for Tau = linspace(0.85,0.35,20)
P(1,1,c) = tf(A,[Tau 1]);
c = c + 1;
end
A = 2.21; 
for Tau = linspace(0.85,0.35,20)
P(1,1,c) = tf(A,[Tau 1]);
c = c + 1;
end
Tau = 0.85; 
for A = linspace(6.60,2.21,20)
P(1,1,c) = tf(A,[Tau 1]);
c = c+1;
end
Tau = 0.35; 
for A = linspace(6.60,2.21,20)
P(1,1,c) = tf(A,[Tau 1]);
c = c + 1;
end

nompt = 73; %nominal plant selection 
%template frequency array 
w = [0.1 1 5 10 100]; %critical_sampling_freq template resembles the w = 100 
sens_specs = 10.^([-20,3,3,3,3]/20);

%check template bounds 
%plottmpl(w,P,nompt);
%ngrid;

%sisobnds(2,[0.5,3,5,10],10.^([-20,3,3,3]/20),P);


% sensitivity design 
bnd1 = sisobnds(2,w,sens_specs,P,0,nompt,1.622); %robust and low frequency specs
bnd2 = sisobnds(2,w,performance_spec,P,0,nompt,1.622); %spec from time specs 
bnds = grpbnds(bnd1, bnd2); %worst case intersections 
worst_bnds = sectbnds(bnds);
%plotbnds(worst_bnds);
%ngrid;
wl = logspace(-2,3,100); %100 points between w = 0.01 and w = 1000 rad/s
L0 = P(1,1,nompt)*1.622; %H = 1.622 - transfer for speed - voltage  
L0.ioDelay = 0; % no delay
C0 = tf(1,1); %no initial controller 

lpshape(wl,worst_bnds,L0,C0);



