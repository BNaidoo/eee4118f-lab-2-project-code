%vars with "pos_' indicated params for the position control system 


%params   
%zeta1 = log(0.25)/sqrt((pi^2)+ log(0.25)^2);

%based on an Mpp of 10%
pos_zeta2 = -log(0.1)/sqrt((pi^2)+ log(0.1)^2);
%based on 5% settling time of 1.5s 
pos_wn = -log(0.05*sqrt(1-pos_zeta2^2))/(pos_zeta2*1.5);
%performance specification bounds (upper)
pos_performance_spec = tf([0 1],[(1/pos_wn)^2 2*pos_zeta2/pos_wn 1]);

G_2 = tf([1/8.71 1],[1/2.87 1]) * 2.545;
H_2 = 1.622;

P_eqv = (G_2*P)/(1 + P*G_2*H_2) * tf([0 1],[1 0]); %eqv loop tf for speed loop * P2 integrator

w = [1 5 10 100]; 
sens_specs = 10.^([3,3,3,3]/20); 

%check template bounds 
%plottmpl(w,P_eqv);
%ngrid;

% sensitivity design 
bnd1 = sisobnds(2,5,10.^(3/20),P_eqv,0,nompt); %robust 
bnd2 = sisobnds(6,w,pos_performance_spec,P_eqv,0,nompt); %spec from time specs 
bnds = grpbnds(bnd1, bnd2); %worst case intersections 
worst_bnds = sectbnds(bnds);

%plotbnds(worst_bnds);
%ngrid;

wl = logspace(-2,3,100); %100 points between w = 0.01 and w = 1000 rad/s
L0 = P_eqv(1,1,nompt); %  
L0.ioDelay = 0; % no delay
C0 = tf(1,1); %no initial controller 

lpshape(wl,worst_bnds,L0,C0);